'use strict'

const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const validator = require('express-validator');
const config = require('./config');
const logger = require('./lib/logger');

/**
 * Import route controllers
 */
const indexRoute = require('./controllers/index.controller');
const registerRoute = require('./controllers/register.controller');
const loginRoute = require('./controllers/login.controller');
const chatRoute = require('./controllers/chat.controller');

/**
 * Connect to database
 */
mongoose.connect(config.MONGO_ADDRESS, (err) => {
	console.log(err); // TODO: handle error
});

/**
 * Middleware
 */
app.use(express.static(__dirname + '/public'));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(validator());

/**
 * Routes
 */
app.use('/', indexRoute);
app.use('/register', registerRoute);
app.use('/login', loginRoute);
app.use('/chat', chatRoute);

/**
 * View settings
 */
app.set('views', __dirname + '/views');
app.set('view engine', 'jade');

if (config.ENVIRONMENT == 'development') {
	app.locals.pretty = true;
}

/**
 * Start app listening on config.PORT
 */
app.listen(config.PORT, () => {
	logger.log('info', 'Server is listening on ' + config.PORT);
});
