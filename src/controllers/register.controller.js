'use strict'

const express = require('express');
const router = express.Router();
const bcrypt = require('bcryptjs');
const logger = require('../lib/logger');
const User = require('../models/User');
const validateUserRequest = require('../lib/validateUserRequest');

/**
 * @route: GET /register
 */
 router.get('/', (req, res) => {
 	res.render('register.jade');
 });

/**
 * @route: POST /register
 */
 router.post('/', (req, res) => {

 	let errors = [];

 	validateUserRequest(req, (err) => {

 		// validate form request
 		if (err) {
 			err.forEach((e) => {
 				errors.push(e.msg);
 			});
 		}

 		// generate password hash
 		let passwordHash = bcrypt.hashSync(req.body.password, bcrypt.genSaltSync(10)); 

		let newUser = new User({
			alias: req.body.alias,
			email: req.body.email,
			password: passwordHash
		});

		// validate against database restraints
		newUser.validate((err) => {
			if (err) {
				if (err.errors.alias) {
					errors.push('Alias is already in use. Please use another.');
				}

				if (err.errors.email) {
					errors.push('Email is already in use. Please use another.');
				}
			} 
			// if no errors, attempt to save
			if (errors.length == 0) {

				newUser.save((err) => {
					if (err) {
						logger.log('error', 'Something went wrong saving a validated user document!');
						errors.push('Something went wrong creating user. Please try again later.');
					}

					else {
						res.render('register.jade', {
							success: true
						});
					}
				});
			}

			if (errors.length != 0) {
				res.render('register.jade', {
					errors: errors,
					alias: req.body.alias,
					email: req.body.email,
					information: req.body.information
				});
			}

		});
 	});

 });


module.exports = router;