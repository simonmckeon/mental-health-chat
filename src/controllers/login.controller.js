'use strict'

const express = require('express');
const router = express.Router();

/**
 * @route: GET /login
 */
router.get('/', (req, res) => {
	res.render('login.jade');
});

module.exports = router;