'use strict'

const express = require('express');
const router = express.Router();

/**
 * @route: GET /chat
 */
router.get('/', (req, res) => {
	res.render('chat.jade');
});

module.exports = router;