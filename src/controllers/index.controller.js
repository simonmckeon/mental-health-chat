'use strict'

const express = require('express');
const router = express.Router();

/**
 * @route: GET /
 */
router.get('/', (req, res) => {
	res.render('index.jade');
});

module.exports = router;