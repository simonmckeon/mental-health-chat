'use strict'

const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const uniqueValidator = require('mongoose-unique-validator');


/**
 * @schema UserSchema
 */
const UserSchema = new Schema({
	alias: { type: String, unique: true },
	email: { type: String, unique: true },
	password: {type: String},
	validated: { type: Boolean, default: false },
	strikes: { type: Number, default: 0 },
	join_date: { type: Date, default: Date.now }
});

UserSchema.plugin(uniqueValidator);

module.exports = mongoose.model('User', UserSchema);