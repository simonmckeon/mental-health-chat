'use strict'

const process = require('process');

module.exports = {
	PORT: process.env.PORT || 3333,
	ENVIRONMENT: process.env.ENVIRONMENT || 'development',
	LOG_DIR: process.env.LOG_DIR || './logs',
	MONGO_ADDRESS: process.env.MONGO_ADDRESS || 'mongodb://localhost/anonchat'
}