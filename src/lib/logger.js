'use strict'

const winston = require('winston');
const moment = require('moment');
const path = require('path');
const config = require('../config');

/**
 * Create new winston instance
 * Always add file transport
 */
let logger = new (winston.Logger)({
	transports: [
		new winston.transports.File({
			filename:  path.resolve(config.LOG_DIR, moment().format('D-M-YYYY_HH:mm:ss') + '.log'),
			timestamp: true,
			json: false
		})
	]
});

/**
 * Add console transport if in development mode
 */
if (config.ENVIRONMENT == 'development') {
	logger.add(winston.transports.Console, {
		colorize: true,
		timestamp: true
	});
}

module.exports = logger;