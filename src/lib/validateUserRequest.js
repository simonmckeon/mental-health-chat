'use strict'

let validateUserRequest = (req, cb) => {
	req.assert('alias', 'Alias is required.').notEmpty();
	req.assert('alias', 'Alias can only contain letters and numbers.').isAlphanumeric();
	req.assert('email', 'A valid email is required.').notEmpty().isEmail();
	req.assert('password', 'Password must be at least 8 characters long.').len(8, 100);
	req.assert('passwordConfirm', 'Passwords do not match.').equals(req.body.password);
	req.assert('information', 'Please tell us a little about you!' ).notEmpty();
	
	let errors = req.validationErrors();

	cb(errors)
}

module.exports = validateUserRequest;